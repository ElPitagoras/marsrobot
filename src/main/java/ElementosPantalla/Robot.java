/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ElementosPantalla;

import DatosExpedicion.Constants;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;

/**
 * Clase que representa y administra al Robot que se verá por pantalla.
 *
 * @author JonathanGarcia
 */
public class Robot {

    private final String rutaImg = Constants.RUTA_FOLDER + "flecha2.jpg";
    private final double height = 100;
    private final double width = 100;
    private ImageView robot;
    private Rectangle mascaraColision;
    private Circle pivote;
    private Rotate rotation;

    private final double limiteIzquierdaX;
    private final double limiteDerechaX;
    private final double limiteArribaY;
    private final double limiteAbajoY;

    /**
     * Constructor Vacio para Robot que asigna los elementos visuales del robot,
     * como los limites de su desplazamiento.
     *
     * @throws FileNotFoundException
     */
    public Robot() throws FileNotFoundException {
        seccionRobot();
        seccionPivote();
        limiteIzquierdaX = 0;
        limiteArribaY = 0;
        limiteDerechaX = Constants.APP_WIDTH - 230;
        limiteAbajoY = Constants.APP_HEIGHT - 130;
    }

    /**
     * Funcion encargada de crear la imagen y su mascara de colision (con la que
     * se comprobará colisiones) al Robot.
     *
     * @throws FileNotFoundException
     */
    private void seccionRobot() throws FileNotFoundException {
        FileInputStream ruta = new FileInputStream(rutaImg);
        Image img = new Image(ruta);
        robot = new ImageView(img);
        robot.setFitHeight(height);
        robot.setFitWidth(width);
        mascaraColision = new Rectangle(width, height, Color.RED);
    }

    /**
     * Funcion encargada de asignar el pivote de giro para que el Robot gire
     * desde su centro.
     */
    private void seccionPivote() {
        pivote = new Circle(0.01);
        pivote.setFill(Color.BLACK);
        pivote.setCenterX(robot.getX() + 0.5 * height);
        pivote.setCenterY(robot.getY() + 0.5 * width);

        rotation = new Rotate();
        rotation.setAngle(0);
        rotation.setPivotX(pivote.getCenterX());//Set the Pivot's X to be the same location as the Circle's X. This is only used to help you see the Pivot's point
        rotation.setPivotY(pivote.getCenterY());

        robot.getTransforms().add(rotation);
        mascaraColision.getTransforms().add(rotation);
    }

    /**
     * Actualiza la posicion de la Imagen del Robot y su mascara de colision por
     * pantalla.
     *
     * @param posX
     * @param posY
     */
    private void actualizarPosicion(double posX, double posY) {
        robot.setLayoutX(posX);
        robot.setLayoutY(posY);
        mascaraColision.setLayoutX(posX);
        mascaraColision.setLayoutY(posY);
    }

    /**
     * Funcion encargada de generar la nueva posicion (X,Y) del Robot a partir
     * de la distancia por recorrer y el angulo actual.
     *
     * @param distancia
     */
    public void desplazarse(double distancia) {
        double desplazX = obtDesplazamientoX(rotation.getAngle(), distancia);
        double desplazY = obtDesplazamientoY(rotation.getAngle(), distancia);
        double validoX = validarEjeX(robot.getLayoutX() + desplazX);
        double validoY = validarEjeY(robot.getLayoutY() + desplazY);
        System.out.println(rotation.getAngle());
        //Corrigiendo angulo si llegó al limite
        double angulo = Math.toDegrees(Math.atan2(validoY - robot.getLayoutY(), validoX - robot.getLayoutX()));
        Platform.runLater(() -> {
            rotation.setAngle(0);
            rotation.setAngle(angulo);
            actualizarPosicion(validoX, validoY);
        });
    }

    /**
     * Funcion encargada de rotar la imagen del Robot una cantidad N de grados.
     *
     * @param angul
     */
    public void rotar(double angul) {
        System.out.println("Angulo Inicial: " + rotation.getAngle());
        double angulo = rotation.getAngle() + angul;
        System.out.println("Angulo final: " + angulo);
        rotation.setAngle(angulo);
    }

    /**
     * Retorna la posicion X a partir de la distancia a recorrer y el angulo de
     * la imagen.
     *
     * @param angulo
     * @param distancia
     * @return
     */
    private double obtDesplazamientoX(double angulo, double distancia) {
        double rad = Math.toRadians(angulo);
        double newX = Math.cos(rad) * distancia;
        return newX;

    }

    /**
     * Retorna la posicion Y a partir de la distancia a recorrer y el angulo de
     * la imagen.
     *
     * @param angulo
     * @param distancia
     * @return
     */
    private double obtDesplazamientoY(double angulo, double distancia) {
        double rad = Math.toRadians(angulo);
        double newY = Math.sin(rad) * distancia;
        return newY;
    }

    /**
     * Retorna la posicion X cuando está dentro de los limites, caso contrario
     * retorna los limites.
     *
     * @param posX
     * @return
     */
    private double validarEjeX(double posX) {
        if (posX < limiteIzquierdaX + 0.001) {
            return limiteIzquierdaX;
        } else if (posX > limiteDerechaX - +0.001) {
            return limiteDerechaX;
        } else {
            return posX;
        }
    }

    /**
     * Retorna la posicion Y cuando está dentro de los limites, caso contrario
     * retorna los limites.
     *
     * @param posY
     * @return
     */
    private double validarEjeY(double posY) {
        if (posY < limiteArribaY + 0.001) {
            return limiteArribaY;
        } else if (posY > limiteAbajoY - +0.001) {
            return limiteAbajoY;
        } else {
            return posY;
        }
    }

    /**
     * Retorna si el robot se encuentra en una esquina.
     *
     * @return
     */
    public boolean enEsquina() {
        boolean ejeX = robot.getLayoutX() == limiteIzquierdaX || robot.getLayoutX() == limiteDerechaX;
        boolean ejeY = robot.getLayoutY() == limiteArribaY || robot.getLayoutY() == limiteAbajoY;
        return ejeX && ejeY;
    }

    /**
     * Retorna la mascara de colision.
     *
     * @return
     */
    public Rectangle getMascaraColision() {
        return mascaraColision;
    }

    /**
     * Retorna la imagen del Robot.
     *
     * @return
     */
    public ImageView getRobot() {
        return robot;
    }

    /**
     * Retorna el pivote de giro.
     *
     * @return
     */
    public Circle getPivote() {
        return pivote;
    }

    /**
     * Retorna el componente Rotate.
     *
     * @return
     */
    public Rotate getRotation() {
        return rotation;
    }

}
