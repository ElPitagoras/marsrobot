package ElementosPantalla;

import DatosExpedicion.DatosCrateres;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Path;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

/**
 * Clase que representa al elemento Crater, sus datos como su imagen.
 *
 * @author JonathanGarcia
 */
public class Crater {// Esta clase se implemento por facilidad de obtener los datos de crateres

    private String idCrater;
    private String nombreCrater;
    private Circle imagen;
    private double radioCrater;
    private double latitud;
    private double longitud;
    private String mineral;
    private int cantidad;
    private boolean sensado;

    private Rectangle robot;

    /**
     * Constructor que recibe la mayoría de parametros del objeto Crater a
     * excepcion de minerales y cantidad que se asignan aleatoriamente.
     *
     * @param idCrater
     * @param nombreCrater
     * @param radioCrater
     * @param latitud
     * @param longitud
     */
    public Crater(String idCrater, String nombreCrater, double radioCrater, double latitud, double longitud) {
        this.idCrater = idCrater;
        this.nombreCrater = nombreCrater;
        this.radioCrater = radioCrater;
        this.latitud = latitud;
        this.longitud = longitud;
        this.imagen = new Circle(latitud, longitud, radioCrater, Color.rgb(230, 90, 90, 0.5));
        this.mineral = DatosCrateres.asignarMineral();
        this.cantidad = DatosCrateres.asiganarCantidadMineral();
    }

    /**
     * Función que asigna la mascara de colisión del Robot al parametro del
     * crater para detectar colisiones.
     *
     * @param robot
     */
    public void setColisionable(Rectangle robot) {
        this.robot = robot;
    }

    /**
     * Retorna si está colisionando con el robot.
     *
     * @return
     */
    public boolean colisionando() {
        return ((Path) (Shape.intersect(robot, imagen))).getElements().size() > 0;
    }

    /**
     * Asigna el valor booleano a sensado.
     *
     * @param valor
     */
    public void setSensado(boolean valor) {
        sensado = valor;
    }

    /**
     * Retorna la imagen (Circle) del crater.
     *
     * @return
     */
    public Circle getImagen() {
        return imagen;
    }

    /**
     * Retorna el ID del crater.
     *
     * @return
     */
    public String getIdCrater() {
        return idCrater;
    }

    /**
     * Retorna el nombre del crater.
     *
     * @return
     */
    public String getNombreCrater() {
        return nombreCrater;
    }

    /**
     * Retorna el radio del crater.
     *
     * @return
     */
    public double getRadioCrater() {
        return radioCrater;
    }

    /**
     * Retorna la latitud del crater.
     *
     * @return
     */
    public double getLatitud() {
        return latitud;
    }

    /**
     * Retorna el mineral que posee.
     *
     * @return
     */
    public String getMineral() {
        return mineral;
    }

    /**
     * Retorna la cantidad en unidades del mineral que posee.
     *
     * @return
     */
    public int getCantidad() {
        return cantidad;
    }

    /**
     * Retorna la longitud del crater.
     *
     * @return
     */
    public double getLongitud() {
        return longitud;
    }

    /**
     * Retorna informacion del crater dependiendo de su parametro sensado.
     *
     * @return
     */
    @Override
    public String toString() {
        if (sensado) {
            return "ID: " + idCrater + ", Nombre: " + nombreCrater + ", Mineral: " + mineral + ", Cantidad: " + cantidad;
        } else {
            return "ID: " + idCrater + ", Nombre: " + nombreCrater;
        }
    }

}
