/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectop2poo.marsrobot;

import DatosExpedicion.Constants;
import DatosExpedicion.DatosCrateres;
import DatosExpedicion.Reportes;
import ElementosPantalla.Crater;
import Vistas.VistaMovimiento;
import Vistas.VistaReporte;
import Vistas.VistaRutas;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

/**
 * JavaFX App
 */
public class App extends Application {

    private Button explorar;
    private Button planificarRu;
    private Button verReportes;
    private Button salir;
    private Scene escenaMenu;
    private Stage stage;

    private ArrayList<Reportes> listaReporte;
    private ArrayList<Crater> listaCrater;

    @Override
    public void init() {
        listaCrater = DatosCrateres.cargarDatosCra();
        listaReporte = new ArrayList();
    }

    @Override
    public void start(Stage moveStage) throws FileNotFoundException {
        stage = moveStage;
        seccionMenu();
        seccionExplorar();
        seccionRutas();
        seccionReportes();
        seccionSalir();
    }

    /**
     * Metodo encargado de la interfaz del Menu Principal.
     *
     * @throws FileNotFoundException
     */
    private void seccionMenu() throws FileNotFoundException {
        FileInputStream file = new FileInputStream(Constants.RUTA_FOLDER + "Nasa.png");
        Image portada = new Image(file);
        ImageView imagen = new ImageView(portada);
        imagen.setFitHeight(200);
        imagen.setFitWidth(200);

        explorar = new Button("Explorar");
        planificarRu = new Button("Planificar Rutas");
        verReportes = new Button("Ver Reportes");
        salir = new Button("Salir");

        FlowPane root = new FlowPane(Orientation.VERTICAL);
        root.setVgap(10);
        root.setAlignment(Pos.CENTER);
        root.getChildren().addAll(imagen, explorar, planificarRu, verReportes, salir);
        Scene escena = new Scene(root, 350, 200);
        escenaMenu = escena;
        stage.setScene(escena);
        stage.setTitle("Robot T2006");
        stage.show();
    }

    /**
     * Metodo encargado de la vista de Exploracion.
     */
    private void seccionExplorar() {
        explorar.setOnMouseClicked((MouseEvent evento) -> {
            if (evento.getButton().equals(MouseButton.PRIMARY)) {
                VistaMovimiento mv = null;
                try {
                    mv = new VistaMovimiento(listaCrater, listaReporte);
                    mv.getRoot().setOnKeyPressed((KeyEvent ev) -> {
                        KeyCode k = ev.getCode();
                        if (k.equals(KeyCode.ESCAPE)) {
                            volverMenu();
                        }
                    });
                } catch (FileNotFoundException ex) {
                    ex.printStackTrace();
                }

                Scene scene = new Scene(mv.getRoot(), Constants.APP_WIDTH, Constants.APP_HEIGHT);
                stage.setScene(scene);
                stage.setTitle("Exploraciòn en Marte");
                stage.setResizable(false);
                stage.show();
            }
        });
    }

    /**
     * Metodo encargado de la vista de Rutas.
     */
    private void seccionRutas() {
        planificarRu.setOnMouseClicked((evento) -> {
            if (evento.getButton().equals(MouseButton.PRIMARY)) {
                VistaRutas vr = null;
                vr = new VistaRutas(listaCrater);
                vr.getRoot().setOnKeyPressed((KeyEvent ev) -> {
                    KeyCode k = ev.getCode();
                    if (k.equals(KeyCode.ESCAPE)) {
                        volverMenu();
                    }
                });
                Scene scene = new Scene(vr.getRoot(), 800, 600);
                stage.setScene(scene);
                stage.setTitle("Planificar Rutas");
                stage.setResizable(false);
                stage.show();
            }
        });
    }

    /**
     * Metodo encargado de la vista de Reportes.
     */
    private void seccionReportes() {
        verReportes.setOnMouseClicked((evento) -> {
            if (evento.getButton().equals(MouseButton.PRIMARY)) {
                VistaReporte vr = null;
                vr = new VistaReporte(listaReporte);
                try {
                    vr.getRoot().setOnKeyPressed((KeyEvent ev) -> {
                        KeyCode k = ev.getCode();
                        if (k.equals(KeyCode.ESCAPE)) {
                            volverMenu();
                        }
                    });
                } catch (NullPointerException ex) {

                }
                Scene scene = new Scene(vr.getRoot(), 800, 600);
                stage.setScene(scene);
                stage.setTitle("Ver Reportes");
                stage.setResizable(false);
                stage.show();
            }
        });
    }

    /**
     * Metodo encargado de cerrar la aplicacion correctamente.
     */
    private void seccionSalir() {
        salir.setOnMouseClicked((MouseEvent evento) -> {
            Stage salir = (Stage) stage.getScene().getWindow();
            salir.close();
        });
    }

    /**
     * Metodo que regresa al Menu Principal
     */
    private void volverMenu() {
        stage.setScene(escenaMenu);
    }

    /**
     * Metodo encargado de mostrar una alerta en caso de error.
     *
     * @param mensaje
     */
    public static void mostrarAlerta(String mensaje) {
        Alert alerta = new Alert(Alert.AlertType.INFORMATION);
        alerta.setTitle("Alerta");
        alerta.setHeaderText("Atencion!!");
        alerta.setContentText("Ingrese formato correcto. " + mensaje);
        alerta.showAndWait();
    }

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Retorna la lista de Crateres.
     *
     * @return
     */
    public ArrayList<Crater> getListaCrater() {
        return listaCrater;
    }

    /**
     * Retorna la lista de Reportes.
     *
     * @return
     */
    public ArrayList<Reportes> getListaReporte() {
        return listaReporte;
    }

}
