/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectop2poo.marsrobot;

import java.io.File;
import java.net.URISyntaxException;
import java.security.CodeSource;

/**
 *
 * @author Victor Andres Garcia Romero
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            CodeSource codeSource = Principal.class.getProtectionDomain().getCodeSource();
            File jarFile;
            jarFile = new File(codeSource.getLocation().toURI().getPath());
            String jarDir = jarFile.getParentFile().getPath();
            //System.out.println(jarDir);
            System.setProperty("jarDir",jarDir);
        } catch (URISyntaxException ex) {
            ex.printStackTrace();
        }
        App.main(args);
    }
}
