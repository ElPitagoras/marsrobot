/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DatosExpedicion;

import ElementosPantalla.Crater;
import java.time.LocalDate;

/**
 * Clase que se encarga de manejar los datos que se generan al momento que el
 * Robert sensa un crater
 *
 * @author Johnny Flores
 */
public class Reportes {

    private Crater crater;
    private String fechaSensado;
    private LocalDate fecha; // fecha

    /**
     * Constructor que recibe un objeto de tipo Crater y inicializa las
     * variables de clase.
     *
     * @param crater
     */
    public Reportes(Crater crater) {
        this.fecha = LocalDate.now();
        this.crater = crater;
        this.fechaSensado = fecha.getYear() + "/" + fecha.getMonthValue() + "/" + fecha.getDayOfMonth();
    }

    /* Retorna la fecha en la que fue sensado el crater.
     * @return
     */
    public String getFechaSensado() {
        return fechaSensado;
    }

    /* Retorna el crater que se senso.
     * @return
     */
    public Crater getCraterSensado() {
        return crater;
    }

    /* Retorna un String con los datos del crater.
     * @return
     */
    @Override
    public String toString() {
        return "Reportes{" + "crater=" + crater + ", fechaSensado=" + fechaSensado + ", fecha=" + fecha + '}';
    }

}
