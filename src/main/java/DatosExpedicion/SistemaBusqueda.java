/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DatosExpedicion;

import ElementosPantalla.Crater;
import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Clase que se encargada de realizar la busqueda por filtro puesto por el
 * usuario en la ventana vista reporte.
 *
 * @author Johnny Flores
 */
public class SistemaBusqueda {

    private final StringProperty fechaInicio;
    private final StringProperty nombreCrater;
    private final StringProperty minerales;
    private Reportes reporte;

    /**
     * Constructor que recibe los parametros de busqueda por el que se va a
     * buscar los crateres sensados requeridos.
     *
     * @param fechaInicio
     * @param nombre
     * @param minerales
     */
    public SistemaBusqueda(String fechaInicio, String nombre, String minerales) {
        this.fechaInicio = new SimpleStringProperty(fechaInicio);
        this.nombreCrater = new SimpleStringProperty(nombre);
        this.minerales = new SimpleStringProperty(minerales);
    }

    /**
     * Constructor que recibe un objeto de tipo Reportes.
     *
     * @param reporte
     */
    public SistemaBusqueda(Reportes reporte) {
        this.reporte = reporte;
        this.fechaInicio = new SimpleStringProperty(reporte.getFechaSensado());
        this.nombreCrater = new SimpleStringProperty(reporte.getCraterSensado().getNombreCrater());
        this.minerales = new SimpleStringProperty(reporte.getCraterSensado().getMineral());
    }

    /**
     * Metodo que recibe un string y un ArrayList para despues retornar una
     * lista con los reportes que tienen ese mineral.
     *
     * @param mineral
     * @param listaReporte
     * @return
     */
    public static ArrayList<Reportes> filtrarMineral(String mineral, ArrayList<Reportes> listaReporte) {
        ArrayList<Reportes> reportes = listaReporte;
        ArrayList<Reportes> Reportes_crateres = new ArrayList<>();
        for (Reportes r : reportes) {
            Crater c = r.getCraterSensado();
            if (c.getMineral().equals(mineral)) {
                Reportes_crateres.add(r);
            }
        }

        return Reportes_crateres;

    }

    /**
     * Metodo que recibe un String de la fecha de inicio, fecha fin, el mineral
     * y una lista de reportes me retorna un ArrayList con objetos
     * SistemaBusqueda que cumplen con los parametros pasados
     *
     * @param fechaInicio
     * @param fechaFin
     * @param mineral
     * @param listaReporte
     */
    public static ArrayList<SistemaBusqueda> filtrarRegistros(String fechaInicio, String fechaFin, String mineral, ArrayList<Reportes> listaReporte) {
       ArrayList<Reportes> crateresCoincidentes = filtrarMineral(mineral, listaReporte);
        ArrayList<SistemaBusqueda> filtro_completo = new ArrayList<>();
        String añoI=null,mesI=null,diaI=null,añoF=null,diaF=null,mesF=null;
        if(!fechaInicio.equals("") || !fechaFin.equals((""))){
            if(!fechaInicio.equals("")){
                String fechai[] = fechaInicio.split("/");
                añoI = fechai[0];
                mesI = fechai[1];
                diaI = fechai[2];
            }
            if(!fechaFin.equals((""))){
               String fechaf[] = fechaFin.split("/");
                añoF = fechaf[0];
                mesF = fechaf[1];
                diaF = fechaf[2]; 
            }
        }
        for (Reportes r : crateresCoincidentes) {
            String fecha[] = r.getFechaSensado().split("/");
            String año = fecha[0];
            String mes = fecha[1];
            String dia = fecha[2];
            if(fechaInicio.equals("") || fechaFin.equals((""))){
                filtro_completo.add(new SistemaBusqueda(r));
            }else{
              if (Integer.parseInt(año) >= Integer.parseInt(añoI) && Integer.parseInt(añoF) <= Integer.parseInt(año)) {
                if (Integer.parseInt(mes) >= Integer.parseInt(mesI) && Integer.parseInt(mes) <= Integer.parseInt(mesF)) {
                    if (Integer.parseInt(dia) <= Integer.parseInt(diaF) && Integer.parseInt(dia) >= Integer.parseInt(diaI)) {
                        filtro_completo.add(new SistemaBusqueda(r));
                    }
                }
            }  
            }
            
        }
        return filtro_completo;
    }

    /**
     * Retorna la decha de inicio pasada como parametro al constructor.
     *
     * @return
     */
    public String getFechaInicio() {
        return fechaInicio.get();
    }

    /**
     * Retorna el nombre del crater buscado.
     *
     * @return
     */
    public String getNombreCrater() {
        return nombreCrater.get();
    }

    /**
     * Retorna el nombre del mineral buscado.
     *
     * @return
     */
    public String getMinerales() {
        return minerales.get();
    }

    /**
     * Retorna un objeto de tipo reporte del crater buscado.
     *
     * @return
     */
    public Reportes getReporte() {
        return reporte;
    }

    /**
     * Retorna un String con los datos de la busqueda.
     *
     * @return
     */
    @Override
    public String toString() {
        return "SistemaBusqueda{" + "fechaInicio=" + fechaInicio + ", nombreCrater=" + nombreCrater + ", minerales=" + minerales + ", reporte=" + reporte + '}';
    }

}
