/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DatosExpedicion;

/**
 * Clase que se encarga de contener las constantes que se necesitan en el
 * sistema.
 *
 * @author rociomera
 */
public class Constants {

    public static final double DISTANCE = 10;
    public static final double ANGULO_ROTACION = 15;
    public static final double APP_WIDTH = 1000;
    public static final double APP_HEIGHT = 700;
    /*
    public static final String RUTA_INFO_CRATER = "src\\main\\java\\InformacionCrater\\crateres_info.txt";
    public static final String RUTA_FOLDER = "src\\main\\java\\Imagenes\\";  // sugiero que se ponga una sola ruta para las imagenes 
    public static final String RUTA_IMG_MARTE = "src\\main\\java\\Imagenes\\Marte.jpg";
    */
    public static String RUTA_INFO_CRATER=System.getProperties().getProperty("jarDir")+"/recursos/crateres_info.txt";
    public static String RUTA_FOLDER=System.getProperties().getProperty("jarDir")+"/recursos/imagenes/";
    public static String RUTA_IMG_MARTE=System.getProperties().getProperty("jarDir")+"/recursos/imagenes/Marte.jpg";
}
