/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DatosExpedicion;

import ElementosPantalla.Crater;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Random;

/**
 * Esta clase se encarga de cargar y asignar los datos de cada crater.
 *
 * @author Johnny Flores
 */
public class DatosCrateres {

    private static String rutaInfo = Constants.RUTA_INFO_CRATER; // llamamos a las constantes que estan en Constants
    private static Random rd = new Random();

    /**
     * Metodo que se encarga de cargar los datos txt al sistema y despues
     * retorna un ArrayList de tipo crater con esos datos.
     *
     * @return
     */
    public static ArrayList<Crater> cargarDatosCra() {
        ArrayList<Crater> listaCrateres = new ArrayList<>();// lista de crateres a retornar
        try ( BufferedReader buffer = new BufferedReader(new FileReader(rutaInfo))) {
            String line = null;
            while ((line = buffer.readLine()) != null) { // Con esto nos aseguramos que haya texto en la linea
                String lS[] = line.split(",");// creamos un arreglo con los datos de los crateres separados por coma
                String id = lS[0];
                String nombre = lS[1];
                String radio = lS[4];
                String latitud = lS[2];
                String longitud = lS[3];
                listaCrateres.add(new Crater(id, nombre, Double.parseDouble(radio), Double.parseDouble(latitud), Double.parseDouble(longitud))); // añadimos una instancia de crateres a la lista
            }
        } catch (Exception e) {

        }
        return listaCrateres;
    }

    /**
     * Metodo que retorna un mineral de un crater al azar
     *
     * @return
     */
    public static String asignarMineral() {
        String[] minerales = new String[]{"Hematita", "Sílice", "Feldespato", "Micas", "Caolinita", "Montmorillonitas", "Ilitas", "Sepiolita", "Sepiolita", "Poligorcita", "Hexametafosfato de Sodio", "Tripolifosfato de Sodio", "Gibbsita"};
        return minerales[rd.nextInt(minerales.length)];
    }

    /**
     * Metodo que retorna la cantidad de mineral de crater al azar.
     *
     * @return
     */
    public static int asiganarCantidadMineral() {
        return rd.nextInt(25) + 5;
    }
}
