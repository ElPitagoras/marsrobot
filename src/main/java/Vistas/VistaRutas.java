/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import DatosExpedicion.DatosCrateres;
import ElementosPantalla.Crater;
import java.util.ArrayList;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 * Clase que simula la vista de Planificacion de Rutas del programa.
 *
 * @author JonathanGarcia
 */
public class VistaRutas {

    public TextField crateresVisitar;
    public TextArea ordenVisitar;
    public VBox _root;
    public ArrayList<Crater> listaCrater;

    /**
     * Constructor vacio para inicializar valores.
     *
     * @param listaCrater
     */
    public VistaRutas(ArrayList<Crater> listaCrater) {
        _root = new VBox();
        this.listaCrater = listaCrater;
        interfaz();
        comandos();
    }

    /**
     * Funcion encargada de crear y mostrar los elementos por pantalla.
     */
    private void interfaz() {
        _root.setAlignment(Pos.CENTER);
        _root.setSpacing(20);

        Label l1 = new Label("Nombre Crateres");
        crateresVisitar = new TextField();
        crateresVisitar.setMaxSize(350, 70);

        ordenVisitar = new TextArea();
        ordenVisitar.setMaxSize(350, 75 * 5);
        ordenVisitar.setEditable(false);

        _root.getChildren().addAll(l1, crateresVisitar, ordenVisitar);
    }

    /**
     * Devuelve el root
     *
     * @return
     */
    public Pane getRoot() {
        return _root;
    }

    /**
     * Funcion encargada de asignar los comandos a los elementos visuales.
     */
    private void comandos() {
        crateresVisitar.setOnKeyPressed((ev) -> {
            KeyCode k = ev.getCode();
            if (k.equals(KeyCode.ENTER)) {
                planificarRutas(crateresVisitar.getText());
            }
        });
    }

    /**
     * Funcion encargada de planificar la ruta más corta a partir de los
     * crateres pasados.
     *
     * @param crateres
     */
    private void planificarRutas(String crateres) {
        ArrayList<Crater> crateresPorVisitar = crateresVisitar(crateres);
        ArrayList<Crater> listaOrdenVisitar = new ArrayList();
        ArrayList<Crater> copiaCrateresVisitar = (ArrayList<Crater>) crateresPorVisitar.clone();
        double posX = 0;
        double posY = 0;

        for (Crater c1 : crateresPorVisitar) {
            Crater craterCercano = hallarCraterCercano(copiaCrateresVisitar, posX, posY);
            if (craterCercano != null) {
                listaOrdenVisitar.add(craterCercano);
                posX = craterCercano.getLongitud();
                posY = craterCercano.getLatitud();
                copiaCrateresVisitar.remove(craterCercano);
            }
        }
        ordenVisitar.setText("");
        for (Crater c : listaOrdenVisitar) {
            addOrdenVisitar(c.toString());
        }
        crateresVisitar.setText("");
    }

    /**
     * Funcion encargada de añadir el Crater al TextArea de orden por visitar.
     *
     * @param crater
     */
    private void addOrdenVisitar(String crater) {
        if (ordenVisitar.getText().equals("")) {
            ordenVisitar.setText(crater);
        } else {
            ordenVisitar.setText(ordenVisitar.getText() + "\n" + crater);
        }
    }

    /**
     * Funcion encargada de retornar el Crater más cercano a la posicion
     * (posX,posY).
     *
     * @param crateresRestantes
     * @param posX
     * @param posY
     * @return
     */
    private Crater hallarCraterCercano(ArrayList<Crater> crateresRestantes, double posX, double posY) {
        Crater craterCercano = crateresRestantes.get(0);
        double distanciaMenor = calcularDistancia(posX, posY, craterCercano.getLongitud(), craterCercano.getLongitud());
        for (Crater c : crateresRestantes) {
            double distanciaTemp = calcularDistancia(posX, posY, c.getLongitud(), c.getLongitud());
            if (distanciaMenor > distanciaTemp) {
                distanciaMenor = distanciaTemp;
                craterCercano = c;
            }
        }
        return craterCercano;
    }

    /**
     * Funcion que calcula la distancia entre dos puntos de la forma (X,Y).
     *
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @return
     */
    private double calcularDistancia(double x1, double y1, double x2, double y2) {
        return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
    }

    /**
     * Funcion que retorna un ArrayList<Crater> a partir de un String de nombre
     * de Crateres.
     *
     * @param crateres
     * @return
     */
    private ArrayList<Crater> crateresVisitar(String crateres) {
        ArrayList<Crater> crateresPorVisitar = new ArrayList();
        String[] crater = crateres.split(",");
        for (String c1 : crater) {
            Crater craterActual = encontrarCrater(c1.trim());
            if (craterActual != null) {
                crateresPorVisitar.add(craterActual);
            }
        }
        return crateresPorVisitar;
    }

    /**
     * Funcion que retorna un objeto del tipo Crater a partir de su nombre.
     *
     * @param nombre
     * @return
     */
    private Crater encontrarCrater(String nombre) {
        for (Crater c : listaCrater) {
            if (c.getNombreCrater().toLowerCase().equals(nombre.toLowerCase())) {
                return c;
            }
        }
        return null;
    }
}
