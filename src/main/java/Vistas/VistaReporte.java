/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import DatosExpedicion.Reportes;
import DatosExpedicion.SistemaBusqueda;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import proyectop2poo.marsrobot.App;

/**
 * Clase que simula los vista de reportes hechos por el Robert.
 *
 * @author JonathanGarcia
 */
public class VistaReporte {

    private VBox _root;
    private TextField inicio;
    private TextField fin;
    private TextField mineral;
    private Button consultar;
    private ArrayList<Reportes> listaReporte;
    private ObservableList<SistemaBusqueda> listaIngresoTabla;

    /**
     * Constructor que recibe una lista de reporte y se encarga de inicilizar el
     * contenedor, la tabla y las variables de clase.
     *
     * @param listaReporte
     */
    public VistaReporte(ArrayList<Reportes> listaReporte) {
        this.listaReporte = listaReporte;
        _root = new VBox();
        _root.setAlignment(Pos.CENTER);
        seccionInputs();
        listaIngresoTabla = generarTablaReporte();
    }

    /**
     * Metodo que muestra los Labels y TextField donde el usuario va a ingresar
     * los parametros de busqueda.
     */
    private void seccionInputs() {
        GridPane inputs = new GridPane();
        inputs.setPadding(new Insets(20));

        Label fecIni = new Label("Fecha Inicio");
        Label fecFin = new Label("Fecha Fin");
        Label miner = new Label("Mineral");

        inicio = new TextField();
        fin = new TextField();
        mineral = new TextField();
        consultar = new Button("Consultar");

        inputs.add(fecIni, 0, 0);
        inputs.add(inicio, 1, 0);
        inputs.add(fecFin, 0, 1);
        inputs.add(fin, 1, 1);
        inputs.add(miner, 0, 2);
        inputs.add(mineral, 1, 2);
        inputs.add(consultar, 2, 1);
        inputs.setVgap(10);
        inputs.setHgap(10);

        _root.getChildren().add(inputs);
        ejecutar_Reporte();
    }

    /**
     * Metodo encargado de crear un TableView que retorna un ObservableList con
     * objetos de tipo SistemaBusqueda que son los items del TableView.
     *
     * @return
     */
    private ObservableList<SistemaBusqueda> generarTablaReporte() {
        BorderPane contenedorTabla = new BorderPane();
        TableView<SistemaBusqueda> tabla = new TableView<>();// creamos una instancia de tabla con tipo de dato RegistroExploracion
        ObservableList<SistemaBusqueda> registros = FXCollections.observableArrayList(); // creamos un observable list para registros
        tabla.setItems(registros);// a medida que agreguemos elementos a registros(Obserbable list) van aunmentando los items
        tabla.setMaxSize(500, 600);// tamaño maximo
        tabla.setMinSize(250, 300);// tamaño minimo
        TableColumn<SistemaBusqueda, String> columna1 = new TableColumn("Fecha Exploracion");// Creamos un TableColum con respecto a la clase RegistroExploracion, con datos String
        columna1.setCellValueFactory(new PropertyValueFactory<SistemaBusqueda, String>("fechaInicio"));
        TableColumn<SistemaBusqueda, String> columna2 = new TableColumn("Nombre del crater");
        columna2.setCellValueFactory(new PropertyValueFactory<SistemaBusqueda, String>("nombreCrater"));

        TableColumn<SistemaBusqueda, String> columna3 = new TableColumn("Mineral");
        columna3.setCellValueFactory(new PropertyValueFactory<SistemaBusqueda, String>("minerales"));

        tabla.getColumns().addAll(columna1, columna2, columna3);
        contenedorTabla.setCenter(tabla);
        _root.getChildren().add(contenedorTabla);
        _root.setAlignment(Pos.CENTER);
        return registros;
    }

    /**
     * Metodo que se encarga de agregar items al TableView.
     */
    private void agregarItems() {
        try{
            if ( mineral.getText().equals("")){
                App.mostrarAlerta("No ha ingresado el mineral");
            }else if (listaReporte.size()==0 ){
                App.mostrarAlerta("No ha hecho ningun senso");
            }else{
                ArrayList<SistemaBusqueda> crateresCoin = SistemaBusqueda.filtrarRegistros(inicio.getText(), fin.getText(), mineral.getText(), listaReporte);
                for (SistemaBusqueda r : crateresCoin) {
                    listaIngresoTabla.add(r);   
                }
            } 
        }catch(ArrayIndexOutOfBoundsException ex){
            App.mostrarAlerta("Formato de fecha Incorrecta, ejm: 2020/09/04");
        }
    }
    /**
     * Metodo que se encarga de manejar el evento del boton consultar.
     */
    private void ejecutar_Reporte() {
        consultar.setOnMouseClicked((evento) -> {
            listaIngresoTabla.clear();
            if (evento.getButton().equals(MouseButton.PRIMARY)) {
                agregarItems();
            }
        });
    }

    /**
     * Metod que retorna el contenedor raiz.
     *
     * @return
     */
    public Pane getRoot() {
        return _root;
    }
}
