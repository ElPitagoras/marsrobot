/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import DatosExpedicion.Constants;
import ElementosPantalla.Crater;
import ElementosPantalla.Robot;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import DatosExpedicion.Reportes;
import static java.lang.Thread.sleep;
import proyectop2poo.marsrobot.App;

/**
 * Clase que simula la exploracion del Robot en Marte.
 *
 * @author Victor Andres Garcia Romero
 */
public class VistaMovimiento {

    private BorderPane _root;
    private Pane _contenedorPng;
    private VBox _contenedorComando;
    private TextField txtcomando;
    private TextArea txtdescrip;
    private final ArrayList<Crater> listaCrater;
    private Robot mars;
    private Label infoCrater;
    private ArrayList<Reportes> listaReporte;

    /**
     * Constructor que asigna la lista de Crateres y Reportes que se maneja de
     * forma compartida por las vistas desde App.
     *
     * @param listaCrater
     * @param listaReporte
     * @throws FileNotFoundException
     */
    public VistaMovimiento(ArrayList<Crater> listaCrater, ArrayList<Reportes> listaReporte) throws FileNotFoundException {
        this.listaReporte = listaReporte;
        this.listaCrater = listaCrater;
        mars = new Robot();
        _root = new BorderPane();
        seccionDatos();
        seccionImagen();
        seccionControles();
        seccionCrateres();
        seccionEjecucion();
    }

    /**
     * Retorna el root de la vista.
     *
     * @return
     */
    public Pane getRoot() {
        return _root;
    }

    /**
     * Metodo encargado de mostrar la interfaz para la muestra de datos del
     * crater.
     */
    private void seccionDatos() {
        infoCrater = new Label("Datos Crater: ");
        HBox hbox = new HBox(infoCrater);
        hbox.setAlignment(Pos.BASELINE_LEFT);
        hbox.setPadding(new Insets(8));
        _root.setBottom(hbox);

    }

    /**
     * Metodo encargado de cargar los crateres y mostrarlos por pantalla.
     */
    private void seccionCrateres() {
        for (Crater c : listaCrater) {
            c.getImagen().setOnMousePressed((ev) -> {
                addInfoCrater(c.toString());
            });
            c.setColisionable(mars.getMascaraColision());
            _contenedorPng.getChildren().add(c.getImagen());
        }
    }

    /**
     * Metodo encargado de mostrar por pantalla la imagen de Marte de fondo.
     *
     * @throws FileNotFoundException
     */
    private void seccionImagen() throws FileNotFoundException {
        _contenedorPng = new Pane();
        _contenedorPng.getChildren().add(mars.getMascaraColision());
        _contenedorPng.getChildren().add(mars.getRobot());
        _contenedorPng.getChildren().add(mars.getPivote());

        StackPane _contenedorfondo = new StackPane();

        FileInputStream ruta2 = new FileInputStream(Constants.RUTA_IMG_MARTE);
        ImageView imgMarte = new ImageView(new Image(ruta2));
        _contenedorfondo.setAlignment(Pos.TOP_LEFT);
        imgMarte.setFitHeight(Constants.APP_HEIGHT - 30);
        imgMarte.setFitWidth(Constants.APP_WIDTH - 130);

        _contenedorfondo.getChildren().addAll(imgMarte, _contenedorPng);
        _root.setCenter(_contenedorfondo);
    }

    /**
     * Metodo encargado de mostrar la interfaz para los inputs del usuario.
     */
    private void seccionControles() {
        Label comando = new Label("Ingrese comando:");
        txtcomando = new TextField();
        txtcomando.setMaxSize(110, 70);

        Label comandos = new Label("Comandos Ingresados:");
        txtdescrip = new TextArea();
        txtdescrip.setMaxSize(110, 300);
        txtdescrip.setEditable(false);

        _contenedorComando = new VBox();
        _contenedorComando.getChildren().addAll(comando, txtcomando, comandos, txtdescrip);
        _root.setRight(_contenedorComando);

    }

    /**
     * Metodo encargado de asignar los Eventos a los nodos por pantalla.
     */
    private void seccionEjecucion() {
        txtcomando.setOnKeyPressed((KeyEvent ev) -> {
            KeyCode k = ev.getCode();
            if (k.equals(KeyCode.ENTER)) {
                String text = txtcomando.getText().trim().toLowerCase();
                txtcomando.setText("");
                if (text.startsWith("girar")) {
                    try {
                        if (text.length() > 5 && text.substring(5, 6).equals(":")) {
                            double grados = Double.parseDouble(text.substring(6, text.length()).trim());
                            rotar(grados);
                        } else if (text.equals("girar".toLowerCase())) {
                            rotar(Constants.ANGULO_ROTACION);
                        } else {
                            App.mostrarAlerta("No se olvide que el formato es con dos puntos, es decir:\n 'girar:#grados' ");
                        }
                    } catch (Exception e) {
                        App.mostrarAlerta("Posiblemente puso letra en vez de numero en los grados");
                    }
                } else if (text.equals("sensar")) {
                    sensar();
                } else if (text.equals("avanzar")) {
                    desplazarse(Constants.DISTANCE);
                    addDescripcion("Avanzar");
                } else if (text.startsWith("desplazarse")) {
                    double newX, newY;
                    String[] partes;
                    partes = (text.substring(12, text.length())).split(",");
                    if (text.contains(":")) {
                        try {
                            newX = Double.parseDouble(partes[0]);
                            newY = Double.parseDouble(partes[1]);
                            desplazarse(newX, newY);
                        } catch (Exception ex) {
                            App.mostrarAlerta("Las coordenadas tienen que ser tipo: coordenadax,coordenaday");
                        }
                    } else {
                        App.mostrarAlerta("Recuerde poner los dos puntos antes de la coordenada: 'desplazarse:coordenadax,coordenaday'");
                    }
                }
            }
        });
    }

    /**
     * Funcion encargada de desplazar una distancia N siguiendo el angulo de
     * vision actual del Robot.
     *
     * @param distancia
     */
    public void desplazarse(double distancia) {
        mars.desplazarse(distancia);
        //System.out.println("X: " + mars.getRobot().getLayoutX() + " Y: " + mars.getRobot().getLayoutY());
    }

    /**
     * Funcion que desplaza al Robot hasta una posicion (X,Y)
     *
     * @param posX
     * @param posY
     * @throws InterruptedException
     */
    private void desplazarse(double posX, double posY) throws InterruptedException {
        double NewX = posX - mars.getRobot().getLayoutX();
        double NewY = posY - mars.getRobot().getLayoutY();
        mars.getRotation().setAngle(0);
        double angulo = Math.toDegrees(Math.atan2(NewY, NewX));//Devuelve el �ngulo theta de la conversi�n de coordenadas rectangulares (, ) a coordenadas polares (r, theta).xy
        //System.out.println("Angulo de rotacion: " + angulo);
        rotar(angulo);
        double hipotenusa = Math.sqrt(Math.pow(NewX, 2) + Math.pow(NewY, 2));
        //System.out.println("Hipotenusa: " + hipotenusa);
        Thread hilo = new Thread(new HiloImagen(hipotenusa));
        hilo.start();
        addDescripcion("Desplazarse");
    }

    /**
     * Funcion que rota el robot N grados.
     *
     * @param angulo
     */
    private void rotar(double angulo) {
        mars.rotar(angulo);
        //System.out.println(mars.getRobot().getLayoutX() + ", " + mars.getRobot().getLayoutY());
        addDescripcion("Girar");
    }

    /**
     * Funcion que sensa un crater si el robot esta dentro de su area.
     */
    private void sensar() {
        for (Crater c : listaCrater) {
            if (c.colisionando()) {
                c.getImagen().setFill(Color.rgb(133, 213, 97, 0.5));
                c.setSensado(true);
                listaReporte.add(new Reportes(c));
            }
        }
        addDescripcion("Sensar");
    }

    /**
     * Funcion que añade la funcion al historico.
     *
     * @param txt
     */
    private void addDescripcion(String txt) {
        txtdescrip.setText(txt + "\n" + txtdescrip.getText());
        txtcomando.setText("");
    }

    /**
     * Funcion que muestra los datos del crater por pantalla.
     *
     * @param txt
     */
    private void addInfoCrater(String txt) {
        infoCrater.setText("Datos Crater: " + txt);
    }

    /**
     * Retorna el Robot de la vista actual.
     *
     * @return
     */
    public Robot getRobot() {
        return mars;
    }

    /**
     * Clase privada encargada de desplazar el robot de forma secuencial
     * mediante un hilo.
     */
    private class HiloImagen extends Thread {

        private double hipotenusa;

        /**
         * Constructor que asigna la hipotenusa (distancia) que debe recorre el
         * robot.
         *
         * @param hipotenusa
         */
        public HiloImagen(double hipotenusa) {
            super();
            this.hipotenusa = hipotenusa;
        }

        /**
         * Metodo que desplaza el robot de forma progresiva simulando
         * movimiento.
         */
        @Override
        public void run() {
            //System.out.println("estoy en el run");
            do {
                if (hipotenusa > Constants.DISTANCE) {
                    desplazarse(Constants.DISTANCE);
                    hipotenusa -= Constants.DISTANCE;
                } else {
                    desplazarse(hipotenusa);
                    hipotenusa = 0;
                }
                try {
                    sleep(100);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            } while (hipotenusa > 0 && !(getRobot().enEsquina()));
        }
    }
}
